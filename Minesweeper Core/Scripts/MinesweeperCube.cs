﻿using System.Collections.Generic;
using UnityEngine;

public class MinesweeperCube
{
    
    public enum CubeFace { Front, Back, Left, Right, Top, Bottom, Unknown };
    public enum State { Alive, Dead }
    public enum RevealResult { None, Ok, BombDetonated }
    public int BombCount { get; set; }

    private State gameState;
    public State GameState { get { return gameState; } }

    private Vector3Int tileDimensions;
    public Vector3Int TileDimensions { get { return tileDimensions; } }

    Dictionary<CubeFace, MinesweeperPanel> cubeFaces = new Dictionary<CubeFace, MinesweeperPanel>();
    public Dictionary<CubeFace, MinesweeperPanel> CubeFaces { get { return cubeFaces; } }

    private bool isInitialized = false;
    public bool Initialized { get { return isInitialized; } }

    private int flaggedTileCount = 0;
    public int FlaggedTileCount { get { return flaggedTileCount; } }

    private int incorrectlyFlaggedTileCount = 0;
    public int IncorrectlyFlaggedTileCount { get { return incorrectlyFlaggedTileCount; } }

    public void Create(int tilesX, int tilesY, int tilesZ, int bombCount)
    {
        cubeFaces = new Dictionary<CubeFace, MinesweeperPanel>();

        tileDimensions = new Vector3Int(tilesX, tilesY, tilesZ);
        BombCount = bombCount;

        cubeFaces.Add(CubeFace.Front, new MinesweeperPanel(tileDimensions, CubeFace.Front));
        cubeFaces.Add(CubeFace.Bottom, new MinesweeperPanel(tileDimensions, CubeFace.Bottom));
        cubeFaces.Add(CubeFace.Back, new MinesweeperPanel(tileDimensions, CubeFace.Back));
        cubeFaces.Add(CubeFace.Top, new MinesweeperPanel(tileDimensions, CubeFace.Top));
        cubeFaces.Add(CubeFace.Right, new MinesweeperPanel(tileDimensions, CubeFace.Right));
        cubeFaces.Add(CubeFace.Left, new MinesweeperPanel(tileDimensions, CubeFace.Left));

        flaggedTileCount = 0;
        incorrectlyFlaggedTileCount = 0;
        isInitialized = false;
        gameState = State.Alive;
    }

    public void FlagTile(CubeFace face, int x, int y)
    {
        if (gameState != State.Alive)
        {
            return;
        }

        if (!Initialized)
        {
            Initialize(x,y, face);
        }

        MinesweeperPanel panel = cubeFaces[face];
        MinesweeperTile tile = panel.Tiles[y, x];

        if (tile.State == MinesweeperTile.TileState.Default)
        {
            tile.State = MinesweeperTile.TileState.Flagged;
            panel.SetTileTexture(x, y, TileTexture.Flag);

            if (!tile.IsBomb)
            {
                incorrectlyFlaggedTileCount += 1;
            }

            flaggedTileCount += 1;
        }
        else if (tile.State == MinesweeperTile.TileState.Flagged)
        {
            tile.State = MinesweeperTile.TileState.Default;
            panel.SetTileTexture(x, y, TileTexture.Default);

            if (!tile.IsBomb)
            {
                incorrectlyFlaggedTileCount -= 1;
            }

            flaggedTileCount -= 1;
        }
        
    }

    public void QuestionTile(CubeFace face, int x, int y)
    {
        if (gameState != State.Alive)
        {
            return;
        }

        if (!Initialized)
        {
            Initialize(x, y, face);
        }

        MinesweeperPanel panel = cubeFaces[face];
        MinesweeperTile tile = panel.Tiles[y, x];

        if (tile.State == MinesweeperTile.TileState.Default)
        {
            tile.State = MinesweeperTile.TileState.Question;
            panel.SetTileTexture(x, y, TileTexture.Question);
        }
        else if (tile.State == MinesweeperTile.TileState.Question)
        {
            tile.State = MinesweeperTile.TileState.Default;
            panel.SetTileTexture(x, y, TileTexture.Default);
        }
    }

    public void RevealMap()
    {
        foreach(MinesweeperPanel panel in cubeFaces.Values)
        {
            foreach (MinesweeperTile tile in panel.Tiles)
            {
                if (tile.State == MinesweeperTile.TileState.Flagged)
                {
                    if (!tile.IsBomb)
                    {
                        panel.SetTileTexture(tile.Position.x, tile.Position.y, TileTexture.IncorrectFlag);
                    }
                }
                else if (tile.State == MinesweeperTile.TileState.Default)
                {
                    if (tile.IsBomb)
                    {
                        panel.SetTileTexture(tile.Position.x, tile.Position.y, TileTexture.UndetonatedBomb);
                    }
                    else
                    {
                        RevealSingle(tile);
                    }
                }
            }
        }
    }

    private void KillPlayer()
    {
        gameState = State.Dead;
        RevealMap();
    }

    public RevealResult RevealTile(CubeFace face, int x, int y)
    {
        if (gameState != State.Alive)
        {
            return RevealResult.None;
        }

        if (!Initialized)
        {
            Initialize(x,y, face);
        }

        MinesweeperPanel panel = cubeFaces[face];
        MinesweeperTile tile = panel.Tiles[y, x];

        if (tile.State != MinesweeperTile.TileState.Revealed)
        {
            if (tile.IsBomb)
            {
                tile.State = MinesweeperTile.TileState.Revealed;
                panel.SetTileTexture(x, y, TileTexture.DetonatedBomb);

                KillPlayer();
                return RevealResult.BombDetonated;
            }
            else if (tile.AdjacentBombs > 0)
            {
                RevealSingle(tile);
            }
            else
            {
                RevealRegion(tile);
            }
        }

        return RevealResult.Ok;
    }

    private void RevealSingle(MinesweeperTile tile)
    {
        if (tile.State == MinesweeperTile.TileState.Default && !tile.IsBomb)
        {
            tile.State = MinesweeperTile.TileState.Revealed;
            TileTexture tileTexture = (TileTexture)tile.AdjacentBombs;
            tile.Panel.SetTileTexture(tile.Position.x, tile.Position.y, tileTexture);
        }
    }

    private void RevealRegion(MinesweeperTile tile)
    {
        List<MinesweeperTile> openList = new List<MinesweeperTile>();
        openList.Add(tile);

        while (openList.Count > 0)
        {
            tile = openList[openList.Count - 1];
            openList.RemoveAt(openList.Count - 1);

            RevealSingle(tile);

            if (tile.AdjacentBombs == 0)
            {
                List<MinesweeperTile> adjacent = GetAdjacentTiles(tile);

                foreach (MinesweeperTile neighbor in adjacent)
                {
                    if (neighbor.AdjacentBombs == 0 && neighbor.State == MinesweeperTile.TileState.Default)
                    {
                        openList.Add(neighbor);
                    }
                    else
                    {
                        RevealSingle(neighbor);
                    }
                }
            }
        }
    }

    public MinesweeperPanel GetPanel(string name)
    {
        try
        {
            CubeFace cubeFace = (CubeFace)System.Enum.Parse(typeof(CubeFace), name);
            return cubeFaces[cubeFace];
        }
        catch (System.Exception e)
        {
            return null;
        }
    }

    public MinesweeperPanel GetPanel(CubeFace face)
    {
        MinesweeperPanel panel;

        if(cubeFaces.TryGetValue(face, out panel))
        {
            return panel;
        }
        else
        {
            return null;
        }
    }

    private MinesweeperTile PickRandomTile()
    {
        int tileCount = (2 * tileDimensions.z * tileDimensions.x) + (2 * tileDimensions.z * tileDimensions.y) + (2 * tileDimensions.x * tileDimensions.y);
        int tileIndex = Random.Range(0, tileCount);

        foreach (CubeFace cubeFace in System.Enum.GetValues(typeof(CubeFace)))
        {
            MinesweeperPanel panel = GetPanel(cubeFace);
            if (tileIndex < panel.TileCount)
            {
                return panel.GetTile(tileIndex % panel.Width, tileIndex / panel.Width);
            }
            else
            {
                tileIndex -= panel.TileCount;
            }
        }

        return null;
    }

    private void Initialize(int initialX, int initialY, CubeFace face)
    {
        Debug.Log("Initializing Room");
        Vector2Int initial = new Vector2Int(initialX, initialY);
        
        int bombsPlaced = 0;

        while (bombsPlaced < BombCount)
        {
            MinesweeperTile tile = PickRandomTile();

            if (tile.Panel.CubeFace == face && Vector2Int.Distance(tile.Position, initial) <= 1.5f)
            {
                continue;
            }

            if (MarkTileAsBomb(tile))
            {
                bombsPlaced += 1;
            }
        }

        isInitialized = true;
    }

    private bool MarkTileAsBomb(MinesweeperTile tile)
    {
        if (!tile.IsBomb)
        {
            tile.IsBomb = true;

            List<MinesweeperTile> adjacentTiles = GetAdjacentTiles(tile);

            foreach (MinesweeperTile adjacentTile in adjacentTiles)
            {
                adjacentTile.AdjacentBombs += 1;
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    private List<MinesweeperTile> GetAdjacentTiles(MinesweeperTile tile)
    {
        List<MinesweeperTile> tiles = new List<MinesweeperTile>();

        int[] xMod = { -1, 0, 1 };
        int[] yMod = { -1, 0, 1 };

        foreach (int dy in yMod)
        {
            foreach (int dx in xMod)
            {

                if ((tile.Position.x + dx < 0 && tile.Position.y + dy < 0) || //guard bottom left corner
                    (tile.Position.x + dx < 0 && tile.Position.y + dy >= tile.Panel.Height) || //guard top left corner
                    (tile.Position.x + dx >= tile.Panel.Width && tile.Position.y + dy < 0) || // guard bottom right corner
                    (tile.Position.x + dx >= tile.Panel.Width && tile.Position.y + dy >= tile.Panel.Height)) //guard top right corner
                {
                    continue;
                }
                CubeFace face = tile.Panel.CubeFace;
                Vector2Int position = tile.Position;

                if (position.y + dy < 0)
                {
                    position.x += dx;
                    MinesweeperUtil.GetBelow(this, ref face, ref position);
                }
                else if (position.y + dy >= GetPanel(face).Height)
                {
                    position.x += dx;
                    MinesweeperUtil.GetAbove(this, ref face, ref position);
                }
                else if (position.x + dx < 0)
                {
                    position.y += dy;
                    MinesweeperUtil.GetLeft(this, ref face, ref position);
                }
                else if (position.x + dx >= GetPanel(face).Width)
                {
                    position.y += dy;
                    MinesweeperUtil.GetRight(this, ref face, ref position);
                }
                else
                {
                    position.x += dx;
                    position.y += dy;
                }

                tiles.Add(GetPanel(face).GetTile(position));
            }
        }

        return tiles;
    }
}
