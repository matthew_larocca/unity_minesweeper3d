﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinesweeperUtil {
    public static void GetAbove(MinesweeperCube cube, ref MinesweeperCube.CubeFace face, ref Vector2Int position)
    {
        switch (face)
        {
            case MinesweeperCube.CubeFace.Bottom:
                face = MinesweeperCube.CubeFace.Front;
                position.Set(position.x, 0);
                break;

            case MinesweeperCube.CubeFace.Front:
                face = MinesweeperCube.CubeFace.Top;
                position.Set(position.x, 0);
                break;

            case MinesweeperCube.CubeFace.Back:
                MinesweeperPanel top = cube.GetPanel(MinesweeperCube.CubeFace.Top);
                face = MinesweeperCube.CubeFace.Top;
                position.Set(top.Width - 1 - position.x, top.Height - 1);
                break;

            case MinesweeperCube.CubeFace.Top:
                MinesweeperPanel back = cube.GetPanel(MinesweeperCube.CubeFace.Back);
                face = MinesweeperCube.CubeFace.Back;
                position.Set(back.Width - 1 - position.x, back.Height - 1);
                break;

            case MinesweeperCube.CubeFace.Right:
                face = MinesweeperCube.CubeFace.Top;
                position.Set(position.y, position.x);
                break;

            case MinesweeperCube.CubeFace.Left:
                top = cube.GetPanel(MinesweeperCube.CubeFace.Top);
                face = MinesweeperCube.CubeFace.Top;
                position.Set(0, top.Height - 1 - position.x);
                break;
        }
    }

    public static void GetBelow(MinesweeperCube cube, ref MinesweeperCube.CubeFace face, ref Vector2Int position)
    {
        switch (face)
        {
            case MinesweeperCube.CubeFace.Bottom:
                MinesweeperPanel back = cube.GetPanel(MinesweeperCube.CubeFace.Back);
                face = MinesweeperCube.CubeFace.Back;
                position.Set(back.Width - 1 - position.x, 0);
                break;

            case MinesweeperCube.CubeFace.Front:
                MinesweeperPanel bottom = cube.GetPanel(MinesweeperCube.CubeFace.Bottom);
                face = MinesweeperCube.CubeFace.Bottom;
                position.Set(position.x, bottom.Height - 1);
                break;

            case MinesweeperCube.CubeFace.Back:
                bottom = cube.GetPanel(MinesweeperCube.CubeFace.Bottom);
                face = MinesweeperCube.CubeFace.Bottom;
                position.Set(bottom.Width - 1 - position.x, 0);
                break;

            case MinesweeperCube.CubeFace.Top:
                MinesweeperPanel front = cube.GetPanel(MinesweeperCube.CubeFace.Front);
                face = MinesweeperCube.CubeFace.Front;
                position.Set(position.x, front.Height - 1);
                break;

            case MinesweeperCube.CubeFace.Right:
                bottom = cube.GetPanel(MinesweeperCube.CubeFace.Bottom);
                face = MinesweeperCube.CubeFace.Bottom;
                position.Set(bottom.Width - 1, bottom.Height - 1 - position.x);
                break;

            case MinesweeperCube.CubeFace.Left:
                face = MinesweeperCube.CubeFace.Bottom;
                position.Set(0, position.x);
                break;
        }
    }

    public static void GetRight(MinesweeperCube cube, ref MinesweeperCube.CubeFace face, ref Vector2Int position)
    {
        switch (face)
        {
            case MinesweeperCube.CubeFace.Bottom:
                MinesweeperPanel right = cube.GetPanel(MinesweeperCube.CubeFace.Right);
                face = MinesweeperCube.CubeFace.Right;
                position.Set(right.Width - 1 - position.y, 0);
                break;

            case MinesweeperCube.CubeFace.Front:
                face = MinesweeperCube.CubeFace.Right;
                position.Set(0, position.y);
                break;

            case MinesweeperCube.CubeFace.Back:
                face = MinesweeperCube.CubeFace.Left;
                position.Set(0, position.y);
                break;

            case MinesweeperCube.CubeFace.Top:
                right = cube.GetPanel(MinesweeperCube.CubeFace.Right);
                face = MinesweeperCube.CubeFace.Right;
                position.Set(position.y, right.Height - 1);
                break;

            case MinesweeperCube.CubeFace.Right:
                face = MinesweeperCube.CubeFace.Back;
                position.Set(0, position.y);
                break;

            case MinesweeperCube.CubeFace.Left:
                face = MinesweeperCube.CubeFace.Front;
                position.Set(0, position.y);
                break;
        }
    }

    public static void GetLeft(MinesweeperCube cube, ref MinesweeperCube.CubeFace face, ref Vector2Int position)
    {
        switch (face)
        {
            case MinesweeperCube.CubeFace.Bottom:
                face = MinesweeperCube.CubeFace.Left;
                position.Set(position.y, 0);
                break;

            case MinesweeperCube.CubeFace.Front:
                MinesweeperPanel left = cube.GetPanel(MinesweeperCube.CubeFace.Left);
                face = MinesweeperCube.CubeFace.Left;
                position.Set(left.Width - 1, position.y);
                break;

            case MinesweeperCube.CubeFace.Back:
                MinesweeperPanel right = cube.GetPanel(MinesweeperCube.CubeFace.Right);
                face = MinesweeperCube.CubeFace.Right;
                position.Set(right.Width - 1, position.y);
                break;

            case MinesweeperCube.CubeFace.Top:
                left = cube.GetPanel(MinesweeperCube.CubeFace.Left);
                face = MinesweeperCube.CubeFace.Left;
                position.Set(left.Width - 1 - position.y, left.Height - 1);
                break;

            case MinesweeperCube.CubeFace.Right:
                MinesweeperPanel front = cube.GetPanel(MinesweeperCube.CubeFace.Front);
                face = MinesweeperCube.CubeFace.Front;
                position.Set(front.Width - 1, position.y);
                break;

            case MinesweeperCube.CubeFace.Left:
                MinesweeperPanel back = cube.GetPanel(MinesweeperCube.CubeFace.Back);
                face = MinesweeperCube.CubeFace.Back;
                position.Set(back.Width - 1, position.y);
                break;
        }
    }

    public static float Remap(float input_min, float input_max, float output_min, float output_max, float val)
    {
        float f = (val - input_min) / (input_max - input_min);

        return output_min + f * (output_max - output_min);
    }
}
