﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameWall : MonoBehaviour {

    private BoxCollider myCollider;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        if (!myCollider)
        {
            myCollider = this.GetComponent<BoxCollider>();
        }

        Gizmos.DrawWireCube(myCollider.center, myCollider.size);
    }
}
