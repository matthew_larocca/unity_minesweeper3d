﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinesweeperTile
{
    public enum TileState { Default, Flagged, Revealed, Question };

    public TileState State { get; set; }
    public bool IsBomb { get; set; }

    public int AdjacentBombs { get; set; }

    private MinesweeperPanel panel;
    public MinesweeperPanel Panel { get { return panel; } }

    private Vector2Int position;
    public Vector2Int Position { get { return position; } }

    public MinesweeperTile(MinesweeperPanel panel, Vector2Int position)
    {
        State = TileState.Default;
        AdjacentBombs = 0;

        this.panel = panel;
        this.position = position;
    }
}