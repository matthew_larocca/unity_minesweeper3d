﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinesweeperPanel {
    public Material TileMaterial;
    public Vector3 rotation = Vector3.zero;

    private BoxCollider wall;
    private Vector3 tilePos = Vector3.zero;
    private Mesh tileMesh;
    public Mesh Mesh { get { return tileMesh; } }
    private Vector2[] uvs;

    private MinesweeperTile[,] tiles;
    public MinesweeperTile[,] Tiles { get { return tiles; } }
    public int Width { get { return tiles.GetLength(1); } }
    public int Height { get { return tiles.GetLength(0); } }

    public int TileCount { get { return Width * Height; } }

    private bool meshDirty = false;
    private MinesweeperCube.CubeFace cubeFace;
    public MinesweeperCube.CubeFace CubeFace { get { return cubeFace; } }

    public MinesweeperPanel(Vector3Int dimensions, MinesweeperCube.CubeFace cubeFace)
    {
        this.cubeFace = cubeFace;

        SetupTiles(dimensions, cubeFace);
        SetupWall(dimensions, cubeFace);
        CreateMesh();
        SetRotation(cubeFace);
    }

    void SetupTiles(Vector3Int dimensions, MinesweeperCube.CubeFace cubeFace)
    {
        int width = 0;
        int height = 0;

        switch (cubeFace)
        {
            case MinesweeperCube.CubeFace.Front:
            case MinesweeperCube.CubeFace.Back:
                width = dimensions.x;
                height = dimensions.y;
                break;

            case MinesweeperCube.CubeFace.Bottom:
            case MinesweeperCube.CubeFace.Top:
                width = dimensions.x;
                height = dimensions.z;
                break;

            case MinesweeperCube.CubeFace.Right:
            case MinesweeperCube.CubeFace.Left:
                width = dimensions.z;
                height = dimensions.y;
                break;
        };

        tiles = new MinesweeperTile[height, width];

        for (int h = 0; h < height; ++h)
        {
            for (int w = 0; w < width; ++w)
            {
                tiles[h, w] = new MinesweeperTile(this, new Vector2Int(w, h));
            }
        }
    }

    void SetupWall(Vector3Int dimensions, MinesweeperCube.CubeFace cubeFace)
    {
        Vector3 center = Vector3.zero;
        Vector3 size = Vector3.zero;
        GameObject gameObject = null;
        switch (cubeFace)
        {
            case MinesweeperCube.CubeFace.Front:
                center.Set(0.0f, dimensions.y / 2.0f, (dimensions.z / 2.0f) + 0.5f);
                tilePos.Set(center.x, center.y, (dimensions.z / 2.0f));
                size.Set(dimensions.x, dimensions.y, 1.0f);
                gameObject = GameObject.Find("Front");
                break;

            case MinesweeperCube.CubeFace.Back:
                center.Set(0.0f, dimensions.y / 2.0f, (-dimensions.z / 2.0f) - 0.5f);
                tilePos.Set(center.x, center.y, (-dimensions.z / 2.0f));
                size.Set(dimensions.x, dimensions.y, 1.0f);
                gameObject = GameObject.Find("Back");
                break;

            case MinesweeperCube.CubeFace.Bottom:
                center.Set(0.0f, -0.5f, 0.0f);
                size.Set(dimensions.x, 1.0f, dimensions.z);
                gameObject = GameObject.Find("Bottom");
                break;

            case MinesweeperCube.CubeFace.Top:
                center.Set(0.0f, dimensions.y + 0.5f, 0.0f);
                tilePos.Set(0.0f, dimensions.y, 0.0f);
                size.Set(dimensions.x, 1.0f, dimensions.z);
                gameObject = GameObject.Find("Top");
                break;

            case MinesweeperCube.CubeFace.Right:
                center.Set((dimensions.x / 2.0f) + 0.5f, dimensions.y / 2.0f, 0.0f);
                tilePos.Set(dimensions.x / 2.0f, dimensions.y / 2.0f, 0.0f);
                size.Set(1.0f, dimensions.y, dimensions.z);
                gameObject = GameObject.Find("Right");
                break;

            case MinesweeperCube.CubeFace.Left:
                center.Set((-dimensions.x / 2.0f) - 0.5f, dimensions.y / 2.0f, 0.0f);
                tilePos.Set(-dimensions.x / 2.0f, dimensions.y / 2.0f, 0.0f);
                size.Set(1.0f, dimensions.y, dimensions.z);
                gameObject = GameObject.Find("Left");
                break;
        };

        wall = gameObject.GetComponent<BoxCollider>();
        wall.center = center;
        wall.size = size;

    }

	void CreateMesh() {
        int vertexCount = Width * Height * 4;
        int triangleCount = Width * Height * 6;

        tileMesh = new Mesh();
        tileMesh.MarkDynamic();

        Vector3[] vertices = new Vector3[vertexCount];
        Vector3[] normals = new Vector3[vertexCount];
        for (int i = 0; i < vertexCount; i++)
        {
            normals[i] = Vector3.back;
        }

        uvs = new Vector2[vertexCount];
        int[] triangles = new int[triangleCount];

        float y = -Height / 2.0f;

        int vIndex = 0;
        int tIndex = 0;

        for (int i = 0; i < Height; ++i)
        {
            float x = -Width / 2.0f;

            for (int j = 0; j < Width; ++j)
            {
                int baseVIndex = vIndex;

                vertices[vIndex++] = new Vector3(x, y, 0.0f);
                vertices[vIndex++] = new Vector3(x, y + 1.0f, 0.0f);
                vertices[vIndex++] = new Vector3(x +1.0f, y + 1.0f, 0.0f);
                vertices[vIndex++] = new Vector3(x + 1.0f, y, 0.0f);

                TextureMapLookup.SetTexCoords(TileTexture.Default, baseVIndex, uvs);

                triangles[tIndex++] = baseVIndex;
                triangles[tIndex++] = baseVIndex + 1;
                triangles[tIndex++] = baseVIndex + 2;
                triangles[tIndex++] = baseVIndex + 2;
                triangles[tIndex++] = baseVIndex + 3;
                triangles[tIndex++] = baseVIndex;

                x += 1.0f;
            }

            y += 1.0f;
        }

        tileMesh.vertices = vertices;
        tileMesh.normals = normals;
        tileMesh.uv = uvs;
        tileMesh.triangles = triangles;
    }

    public void SetTileTexture(int x, int y, TileTexture tileType)
    {
        int uvIndex = (Width * 4 * y) + (4 * x);
        TextureMapLookup.SetTexCoords(tileType, uvIndex, uvs);

        meshDirty = true;
    }

    public void UpdateMesh()
    {
        if (meshDirty)
        {
            tileMesh.uv = uvs;
            meshDirty = false;
        }
    }

    private void SetRotation(MinesweeperCube.CubeFace face)
    {
        switch (face)
        {
            case MinesweeperCube.CubeFace.Bottom:
                rotation.x = 90.0f;
                break;

            case MinesweeperCube.CubeFace.Back:
                rotation.y = 180.0f;
                break;

            case MinesweeperCube.CubeFace.Top:
                rotation.x = -90.0f;
                break;

            case MinesweeperCube.CubeFace.Right:
                rotation.y = 90.0f;
                break;

            case MinesweeperCube.CubeFace.Left:
                rotation.y = -90.0f;
                break;
        }
    }
    public MinesweeperTile GetTile(int x, int y)
    {
        return tiles[y, x];
    }

    public MinesweeperTile GetTile(Vector2Int position)
    {
        return tiles[position.y, position.x];
    }

    public Matrix4x4 Matrix { get { return Matrix4x4.TRS(tilePos, Quaternion.Euler(rotation), Vector3.one); } }
}
