﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinesweeperGame {
    private MinesweeperCube cube;

    public MinesweeperCube Minesweeper { get { return cube; } }

    public delegate void TileRevelaedCallback(MinesweeperCube.RevealResult revealResult);
    public event TileRevelaedCallback OnTileRevealed;

    public delegate void GameStartingCallback();
    public event GameStartingCallback OnGameStarted;

    public int BombCount { get { return cube.BombCount; } }

    public MinesweeperGame()
    {
        cube = new MinesweeperCube();
    }

    public MinesweeperCube.RevealResult RevealTileFromRay(Ray ray)
    {
        Vector2Int position;
        MinesweeperCube.CubeFace cubeFace;

        if (PickTile(ray, out position, out cubeFace))
        {
            MinesweeperCube.RevealResult result =  cube.RevealTile(cubeFace, position.x, position.y);

            if (OnTileRevealed != null)
            {
                OnTileRevealed(result);
            }
            
            return result;
        }
        else
        {
            return MinesweeperCube.RevealResult.None;
        }
    }

    public void FlagTileFromRay(Ray ray)
    {
        Vector2Int position;
        MinesweeperCube.CubeFace cubeFace;

        if (PickTile(ray, out position, out cubeFace))
        {
            cube.FlagTile(cubeFace, position.x, position.y);
        }
    }

    public void QuestionTileFromRay(Ray ray)
    {
        Vector2Int position;
        MinesweeperCube.CubeFace cubeFace;

        if (PickTile(ray, out position, out cubeFace))
        {
            cube.QuestionTile(cubeFace, position.x, position.y);
        }
    }

    private bool PickTile(Ray ray, out Vector2Int position, out MinesweeperCube.CubeFace cubeFace)
    {
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            MinesweeperPanel panel = cube.GetPanel(hit.transform.name);

            if (panel != null)
            {
                //Map the world space hit point to the object space hit point
                Matrix4x4 invMatrix = panel.Matrix.inverse;
                Vector3 localPos = invMatrix.MultiplyPoint(hit.point);

                //This maps the hit point from local object space of the panel to the 2D cell grid index.
                Vector2Int gridPos = new Vector2Int(
                    Mathf.FloorToInt(MinesweeperUtil.Remap(-panel.Width / 2.0f, panel.Width / 2.0f, 0, panel.Width, localPos.x)),
                    Mathf.FloorToInt(MinesweeperUtil.Remap(-panel.Height / 2.0f, panel.Height / 2.0f, 0, panel.Height, localPos.y))
                    );

                Debug.Log(hit.transform.name + ": " + gridPos.ToString());

                position = gridPos;
                cubeFace = panel.CubeFace;
                return true;
            }
            else
            {
                position = Vector2Int.zero;
                cubeFace = MinesweeperCube.CubeFace.Unknown;
                return false;
            }
        }
        else
        {
            position = Vector2Int.zero;
            cubeFace = MinesweeperCube.CubeFace.Unknown;
            return false;
        }
    }

    public void NewBeginnerGame()
    {
        Minesweeper.Create(10, 10, 10, 100);
        
        if (OnGameStarted != null)
        {
            OnGameStarted();
        }

    }

    public void NewIntermediateGame()
    {
        Minesweeper.Create(16, 10, 16, 180);

        if (OnGameStarted != null)
        {
            OnGameStarted();
        }
    }

    public void NewAdvancedGame()
    {
        Minesweeper.Create(16, 10, 30, 400);

        if (OnGameStarted != null)
        {
            OnGameStarted();
        }
    }
}
