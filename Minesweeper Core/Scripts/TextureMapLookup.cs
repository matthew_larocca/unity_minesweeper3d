﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileTexture
{
    Empty, One, Two, Three, Four, Five, Six, Seven, Eight, Flag, Default, UndetonatedBomb, DetonatedBomb, Question, IncorrectFlag
}

public class TextureMapLookup{
    static Dictionary<TileTexture, Vector2Int> tileCoordinates;
    static Vector2 tileDimensions;

    static TextureMapLookup()
    {
        tileCoordinates = new Dictionary<TileTexture, Vector2Int>();
        tileDimensions = new Vector2(1.0f / 16.0f, 1.0f);

        tileCoordinates.Add(TileTexture.Default, new Vector2Int(0, 0));
        tileCoordinates.Add(TileTexture.Flag, new Vector2Int(1, 0));
        tileCoordinates.Add(TileTexture.Empty, new Vector2Int(2, 0));
        tileCoordinates.Add(TileTexture.One, new Vector2Int(3, 0));
        tileCoordinates.Add(TileTexture.Two, new Vector2Int(4, 0));
        tileCoordinates.Add(TileTexture.Three, new Vector2Int(5, 0));
        tileCoordinates.Add(TileTexture.Four, new Vector2Int(6, 0));
        tileCoordinates.Add(TileTexture.Five, new Vector2Int(7, 0));
        tileCoordinates.Add(TileTexture.Six, new Vector2Int(8, 0));
        tileCoordinates.Add(TileTexture.Seven, new Vector2Int(9, 0));
        tileCoordinates.Add(TileTexture.Eight, new Vector2Int(10, 0));
        tileCoordinates.Add(TileTexture.UndetonatedBomb, new Vector2Int(11, 0));
        tileCoordinates.Add(TileTexture.DetonatedBomb, new Vector2Int(12, 0));
        tileCoordinates.Add(TileTexture.Question, new Vector2Int(13, 0));
        tileCoordinates.Add(TileTexture.IncorrectFlag, new Vector2Int(14, 0));
    }

    public static void SetTexCoords(TileTexture tileType, int uvIndex, Vector2[] uvs)
    {
        Vector2Int coords;
        tileCoordinates.TryGetValue(tileType, out coords);

        float minX = coords.x * tileDimensions.x;
        float maxX = (coords.x +1) * tileDimensions.x;

        float minY = coords.y * tileDimensions.y;
        float maxY = (coords.y + 1) * tileDimensions.y;

        uvs[uvIndex++] = new Vector2(minX, minY);
        uvs[uvIndex++] = new Vector2(minX, maxY);
        uvs[uvIndex++] = new Vector2(maxX, maxY);
        uvs[uvIndex++] = new Vector2(maxX, minY);
    }
}
