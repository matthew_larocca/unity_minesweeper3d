﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerUI : MonoBehaviour {

    private Text _text;
    private float gameTime;
    public bool TimerRunning { get; set; }

    private void Awake()
    {
        _text = GetComponent<Text>();
    }

    private void OnEnable()
    {
        gameTime = 0.0f;
        TimerRunning = true;
    }

    // Update is called once per frame
    void Update () {
        if (TimerRunning)
        {
            gameTime += Time.deltaTime;

            int seconds = Mathf.FloorToInt(gameTime);

            int hours = seconds / 3600;
            seconds -= hours * 3600;

            int minutes = seconds / 60;
            seconds -= minutes * 60;

            _text.text = string.Format("{0:D2}h {1:D2}m {2:D2}s", hours, minutes, seconds);
        }
    }
}
