﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BombsRemainingUI : MonoBehaviour {
    private Text _text;

    private MinesweeperCube minesweeper;

    void Awake()
    {
        _text = GetComponent<Text>();
    }

    private void Start()
    {
        minesweeper = FindObjectOfType<Minesweeper3D>().Game.Minesweeper;
    }

    void Update ()
    {
        _text.text = string.Format("Bombs: {0}", minesweeper.BombCount - minesweeper.FlaggedTileCount);
	}
}
