﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycast : MonoBehaviour {

    private Minesweeper3D minesweeper3D;

	void Start () {
        minesweeper3D = GameObject.FindObjectOfType<Minesweeper3D>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            minesweeper3D.RevealTileFromRay(new Ray(this.transform.position, this.transform.forward));
        }
        else if (Input.GetMouseButtonDown(1))
        {
            minesweeper3D.FlagTileFromRay(new Ray(this.transform.position, this.transform.forward));
        }
        else if (Input.GetMouseButtonDown(2))
        {
            minesweeper3D.QuestionTileFromRay(new Ray(this.transform.position, this.transform.forward));
        }
    }
}
