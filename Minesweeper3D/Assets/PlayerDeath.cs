﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDeath : MonoBehaviour {
    public AudioClip CountdownSound;
    public AudioClip ExplosionSound;
    public GameObject DeathFade;

    private AudioSource _audioSource;

    private enum DeathState { Starting, CountingDown, Exploding, Complete}
    private DeathState state = DeathState.Starting;

    public bool FadeInComplete { get; set; }
    public bool FadeOutComplete { get; set; }

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }


    private void OnEnable()
    {
        state = DeathState.Starting;
        FadeInComplete = false;
        FadeOutComplete = false;
    }

    private void OnDisable()
    {
        DeathFade.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
        if (state == DeathState.Starting)
        {
            _audioSource.clip = CountdownSound;
            _audioSource.Play();
            state = DeathState.CountingDown;
        }
		else if (state == DeathState.CountingDown)
        {
            if (!_audioSource.isPlaying)
            {
                _audioSource.clip = ExplosionSound;
                _audioSource.Play();
                state = DeathState.Exploding;
                DeathFade.SetActive(true);
            }
        }
        else if (state == DeathState.Exploding)
        {
            if (!_audioSource.isPlaying)
            {
                state = DeathState.Complete;
            }
        }
	}
}
