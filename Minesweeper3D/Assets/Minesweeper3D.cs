﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Minesweeper3D : MonoBehaviour {
    public Material AliveMaterial;
    public Material DeadMaterial;
    public GameObject PlayerDeathObj;
    public GameObject DifficultySelectPanel;
    public GameObject NextGamePanel;

    public GameObject BombsRemainingPanel;
    public GameObject TimerPanel;

    private MinesweeperGame game;

    private FirstPersonController player;
    private Vector3 initialPlayerPosition;

    private Material tileMaterial;

    public MinesweeperGame Game { get { return game; } }
    private void Awake()
    {
        game = new MinesweeperGame();
    }


    void Start ()
    {
        player = GameObject.Find("Player").GetComponent<FirstPersonController>();
        initialPlayerPosition = player.transform.position;
        NextGame();
    }
	

	void Update ()
    {
        if (PlayerDeathObj.activeInHierarchy)
        {
            PlayerDeath pd = PlayerDeathObj.GetComponent<PlayerDeath>();

            if (tileMaterial == AliveMaterial && pd.FadeInComplete)
            {
                tileMaterial = DeadMaterial;
                player.AudioEnabled = false;
            }

            if (pd.FadeOutComplete)
            {
                NextGamePanel.SetActive(true);
                PlayerDeathObj.SetActive(false);
            }
        }

        if (NextGamePanel.activeInHierarchy)
        {
            if (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1) || Input.GetMouseButtonUp(2))
            {
                NextGame();
            }
        }

        foreach (MinesweeperPanel cubeFace in game.Minesweeper.CubeFaces.Values)
        {
            cubeFace.UpdateMesh();
            Graphics.DrawMesh(cubeFace.Mesh, cubeFace.Matrix, tileMaterial, 0);
        }
    }

    private void SetupForNewGame()
    {
        GameObject uiPanel = GameObject.Find("DifficultySelectPanel");
        uiPanel.SetActive(false);

        GameObject crosshair = GameObject.Find("Crosshair");
        crosshair.SetActive(true);

        TimerPanel.SetActive(true);
        BombsRemainingPanel.SetActive(true);

        player.SetInputEnabled(true);
        player.AudioEnabled = true;
    }
    public void NextGame()
    {
        NextGamePanel.SetActive(false);
        TimerPanel.SetActive(false);
        BombsRemainingPanel.SetActive(false);

        DifficultySelectPanel.SetActive(true);

        player.transform.position = initialPlayerPosition;
        player.transform.rotation = Quaternion.identity;
        foreach (Transform child in player.transform)
        {
            child.rotation = Quaternion.identity;
        }

        tileMaterial = AliveMaterial;
        player.SetInputEnabled(false);
        player.AudioEnabled = false;
        game.Minesweeper.Create(10, 10, 10, 100);
        game.Minesweeper.RevealMap();
    }

    public void NewBeginnerGame()
    {
        SetupForNewGame();
        game.Minesweeper.Create(10, 10, 10, 100);
    }

    public void NewIntermediateGame()
    {
        SetupForNewGame();
        game.Minesweeper.Create(16, 10, 16, 180);
    }

    public void NewAdvancedGame()
    {
        SetupForNewGame();
        game.Minesweeper.Create(16, 10, 30, 400);
    }

    public void RevealTileFromRay(Ray ray)
    {
        MinesweeperCube.RevealResult result = game.RevealTileFromRay(ray);

        if (result == MinesweeperCube.RevealResult.BombDetonated)
        {
            PlayerDeathObj.SetActive(true);
            TimerPanel.GetComponentInChildren<TimerUI>().TimerRunning = false;
        }
    }

    public void FlagTileFromRay(Ray ray)
    {
        game.FlagTileFromRay(ray);
    }

    public void QuestionTileFromRay(Ray ray)
    {
        game.QuestionTileFromRay(ray);
    }
}
