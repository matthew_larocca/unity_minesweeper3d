﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeathFade : MonoBehaviour {

    public GameObject PlayerDeath;

    private PlayerDeath _playerDeath;
    public void Start()
    {
        _playerDeath = PlayerDeath.GetComponent<PlayerDeath>();
    }

    public void OnFadeInComplete()
    {
        _playerDeath.FadeInComplete = true;
    }

    public void OnFadeOutComplete()
    {
        _playerDeath.FadeOutComplete = true;
    }

    public void OnEnable()
    {
        Debug.Log("Death fade enabled!");

        Animator animator = GetComponent<Animator>();
        animator.Rebind();
    }
}
