﻿using UnityEngine;
using VRTK;

public class MinesweeperControllerEvents : MonoBehaviour {

    public MinesweeperVR _minesweeper;
    public PlayerDeath PlayerDeath;
    private VRTK_ControllerEvents _controllerEvents;

    private void Start()
    {
        _controllerEvents = GetComponent<VRTK_ControllerEvents>();

        _controllerEvents.GripReleased += GripPressed;
        _controllerEvents.TouchpadReleased += TouchPadPressed;
        _controllerEvents.TriggerClicked += TriggerClicked;

        PlayerDeath.DeathFade.HeadsetFadeComplete += OnDeathFadeComplete;

        _minesweeper.Game.OnGameStarted += OnNewGameStarted;
    }

    private void GripPressed(object sender, ControllerInteractionEventArgs args)
    {
        _minesweeper.NewIntermediateGame();
    }

    private void TouchPadPressed(object sender, ControllerInteractionEventArgs args)
    {
        Vector2 touchpadAxis = _controllerEvents.GetTouchpadAxis();

        GameObject controller = args.controllerReference.actual;
        Ray ray = new Ray(controller.transform.position, controller.transform.forward);
        _minesweeper.FlagTileFromRay(ray);
    }

    private void TriggerClicked(object sender, ControllerInteractionEventArgs args)
    {
        GameObject controller = args.controllerReference.actual;
        Ray ray = new Ray(controller.transform.position, controller.transform.forward);
        _minesweeper.RevealTileFromRay(ray);
    }

    private void OnDeathFadeComplete(object sender, HeadsetFadeEventArgs e)
    {
        VRTK.VRTK_StraightPointerRenderer pointerRenderer = GetComponent<VRTK_StraightPointerRenderer>();
        pointerRenderer.validCollisionColor = Color.grey;
    }

    private void OnNewGameStarted()
    {
        VRTK.VRTK_StraightPointerRenderer pointerRenderer = GetComponent<VRTK_StraightPointerRenderer>();
        pointerRenderer.validCollisionColor = Color.yellow;
    }
}
