﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinesweeperUICanvas : MonoBehaviour {

    public MinesweeperVR Minesweeper;

    public GameObject MainUIPanel;
    public GameObject DifficultySelectPanel;
    

	public void OnNewBeginnerDifficultyGame()
    {
        Minesweeper.NewBeginnerGame();
        ShowMainPanel();
    }

    public void OnNewIntermediateDifficultyGame()
    {
        Minesweeper.NewIntermediateGame();
        ShowMainPanel();
    }

    public void OnNewAdvancedDifficultyGame()
    {
        Minesweeper.NewAdvancedGame();
        ShowMainPanel();
    }

    public void OnResetButtonClick()
    {
        ShowDifficultySelectPanel();
    }

    public void OnNewGameCancel()
    {
        ShowMainPanel();
    }

    private void ShowMainPanel()
    {
        MainUIPanel.SetActive(true);
        DifficultySelectPanel.SetActive(false);
    }

    private void ShowDifficultySelectPanel()
    {
        MainUIPanel.SetActive(false);
        DifficultySelectPanel.SetActive(true);
    }
}
