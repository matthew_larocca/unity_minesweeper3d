﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class PlayerDeath : MonoBehaviour {
    private const float DeathFadeDurationSeconds = 1.25f;
    private const float DeathUnFadeDurationSeconds = 1.25f;

    public VRTK_HeadsetFade DeathFade { get { return _headsetFade; } }
    private VRTK_HeadsetFade _headsetFade;

    public AudioClip CountdownSound;
    public AudioClip ExplosionSound;

    private AudioSource _audioSource;

    private enum DeathState { Inactive, Starting, CountingDown, Exploding }
    private DeathState state = DeathState.Inactive;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        _headsetFade = GetComponent<VRTK_HeadsetFade>();
        _headsetFade.HeadsetFadeComplete += OnDeathFadeComplete;
        _headsetFade.HeadsetUnfadeComplete += OnDeathUnfadeComplete;
    }

    public void StartDeathSequence()
    {
        state = DeathState.Starting;
    }

    private void OnDeathFadeComplete(object sender, HeadsetFadeEventArgs e)
    {
        _headsetFade.Unfade(DeathUnFadeDurationSeconds);
    }

    private void OnDeathUnfadeComplete(object sender, HeadsetFadeEventArgs e)
    {
        state = DeathState.Inactive;
    }

    void Update ()
    {
        if (state == DeathState.Starting)
        {
            _audioSource.clip = CountdownSound;
            _audioSource.Play();
            state = DeathState.CountingDown;
        }
        else if (state == DeathState.CountingDown)
        {
            if (!_audioSource.isPlaying)
            {
                _audioSource.clip = ExplosionSound;
                _audioSource.Play();
                state = DeathState.Exploding;
                _headsetFade.Fade(Color.white, DeathFadeDurationSeconds);
            }
        }
    }
}
