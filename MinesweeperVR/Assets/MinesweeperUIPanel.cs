﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRTK;

public class MinesweeperUIPanel : MonoBehaviour {

    public Text TimerText;
    public Text BombsRemainingText;

    public MinesweeperVR minesweeper;
    public PlayerDeath PlayerDeath;
    public Button ResetButton;

    private float gameTime;
    public bool TimerRunning { get; set; }

    private Color _initialTextColor;
    private Color _initialActionColor;

    private void Start()
    {
        _initialTextColor = TimerText.color;
        _initialActionColor = ResetButton.colors.pressedColor;

        minesweeper.Game.OnGameStarted += OnGameStarted;
        minesweeper.Game.OnTileRevealed += OnTileRevealed;

        PlayerDeath.DeathFade.HeadsetFadeComplete += OnDeathFadeComplete;

        RestartTimer();
    }

    void Update () {
        //TODO: this needs to be cleaned up
        BombsRemainingText.text = string.Format("{0}", minesweeper.Game.Minesweeper.BombCount - minesweeper.Game.Minesweeper.FlaggedTileCount);
        if (TimerRunning)
        {
            gameTime += Time.deltaTime;

            int seconds = Mathf.FloorToInt(gameTime);

            int hours = seconds / 3600;
            seconds -= hours * 3600;

            int minutes = seconds / 60;
            seconds -= minutes * 60;

            TimerText.text = string.Format("{0:D2}:{1:D2}", minutes, seconds);
        }


    }

    void OnTileRevealed(MinesweeperCube.RevealResult result)
    {
        if (result == MinesweeperCube.RevealResult.BombDetonated)
        {
            TimerRunning = false;
        }
    }

    void OnGameStarted()
    {
        TimerText.color = _initialTextColor;
        BombsRemainingText.color = _initialTextColor;

        SetButtonColors(ResetButton, _initialTextColor, _initialActionColor);
    }

    void RestartTimer()
    {
        gameTime = 0.0f;
        TimerRunning = true;
    }

    Color GetGrayscaleColor(Color color)
    {
        float grayValue = (0.3f * color.r) + (0.59f * color.g) + (0.11f * color.b);
        return  new Color(grayValue, grayValue, grayValue);
    }

    void SetButtonColors(Button button, Color highlightColor, Color pressedColor)
    {
        ColorBlock buttonColors = ResetButton.colors;
        buttonColors.highlightedColor = highlightColor;
        buttonColors.pressedColor = pressedColor;

        ResetButton.colors = buttonColors;
    }

    void OnDeathFadeComplete(object sender, HeadsetFadeEventArgs e)
    {
        // Use Luminosity method to convert color to grayscale.
        Color deadTextColor = GetGrayscaleColor(_initialTextColor);

        TimerText.color = deadTextColor;
        BombsRemainingText.color = deadTextColor;

        SetButtonColors(ResetButton, deadTextColor, GetGrayscaleColor(_initialActionColor));
    }
}
