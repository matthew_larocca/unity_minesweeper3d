﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class MinesweeperVR : MonoBehaviour {

    public Material AliveMaterial;
    public Material DeadMaterial;
    public PlayerDeath PlayerDeath;

    private Material tileMaterial;

    private MinesweeperGame game;
    public MinesweeperGame Game { get { return game; } }
    private void Awake()
    {
        game = new MinesweeperGame();
    }
    

    // Use this for initialization
    void Start ()
    {
        NewIntermediateGame();

        PlayerDeath.DeathFade.HeadsetFadeComplete += OnDeathFadeComplete;
    }

    public void NewBeginnerGame()
    {
        NextGame();
        game.NewBeginnerGame();
    }

    public void NewIntermediateGame()
    {
        NextGame();
        game.NewIntermediateGame();
    }

    public void NewAdvancedGame()
    {
        NextGame();
        game.NewAdvancedGame();
    }

    public void RevealTileFromRay(Ray ray)
    {
        MinesweeperCube.RevealResult result = game.RevealTileFromRay(ray);

        if (result == MinesweeperCube.RevealResult.BombDetonated)
        {
            PlayerDeath.StartDeathSequence();
        }
    }

    public void FlagTileFromRay(Ray ray)
    {
        game.FlagTileFromRay(ray);
    }

    // Update is called once per frame
    void Update () {
        foreach (MinesweeperPanel cubeFace in game.Minesweeper.CubeFaces.Values)
        {
            cubeFace.UpdateMesh();
            Graphics.DrawMesh(cubeFace.Mesh, cubeFace.Matrix, tileMaterial, 0);
        }
    }

    public void NextGame()
    {
        tileMaterial = AliveMaterial;
    }

    private void OnDeathFadeComplete(object sender, HeadsetFadeEventArgs e)
    {
        tileMaterial = DeadMaterial;
    }
}
